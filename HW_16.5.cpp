#include <iostream>
#include <Windows.h>

int main()
{
    const int N = 10;

    SYSTEMTIME Date;
    GetSystemTime(&Date);
    int I = Date.wDay % N;
    int Summ = 0;
 
    int Array[N][N];

    std::cout << "Array:\n" << std::endl;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            Array[i][j] = i + j;
            std::cout << Array[i][j] << " ";
        }
        std::cout << std::endl;
    }

    for (int j = 0; j < N; j++)
    {
        Summ += Array[I][j];
    }

    std::cout << "\nSum of elements in " << I + 1 << " row: " << Summ << std::endl;

    return 0;
}
